import telebot
import os
import texts as txt
from dotenv import load_dotenv

load_dotenv()
telegram_token = os.environ.get('TELEGRAM_TOKEN')

telebot.apihelper.CONNECT_TIMEOUT = 60
bot = telebot.TeleBot(telegram_token)


@bot.message_handler(commands=['start'])
def send_welcome(message):
    main_menu(message)


def main_menu(message):
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    options = [txt.OPTION_1, txt.OPTION_2, txt.OPTION_3, txt.OPTION_4]
    markup.add(*[telebot.types.KeyboardButton(option) for option in options])
    bot.send_message(message.chat.id, txt.WELCOME_MESSAGE, reply_markup=markup)


# Обработчик нажатий на кнопки "Cлужба"
@bot.message_handler(func=lambda message: message.text == txt.OPTION_1)
def service_handler(message):
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    back_button = telebot.types.KeyboardButton(txt.BACK_BUTTON)
    markup.add(back_button)
    bot.send_message(message.chat.id, txt.SERVICE_ANSWER, reply_markup=markup, parse_mode='HTML')


# Обработчик нажатий на кнопки "Центр"
@bot.message_handler(func=lambda message: message.text == txt.OPTION_2)
def center_handler(message):
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    back_button = telebot.types.KeyboardButton(txt.BACK_BUTTON)
    markup.add(back_button)
    bot.send_message(message.chat.id, txt.CENTER_ANSWER, reply_markup=markup, parse_mode='HTML')


# Обработчик нажатий на кнопки "Электродепо"
@bot.message_handler(func=lambda message: message.text == txt.OPTION_3)
def depot_handler(message):
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    back_button = telebot.types.KeyboardButton(txt.BACK_BUTTON)
    markup.add(back_button)
    bot.send_message(message.chat.id, txt.DEPO_ANSWER, reply_markup=markup, parse_mode='HTML')


# Обработчик нажатий на кнопки "Дирекция Инфраструктуры"
@bot.message_handler(func=lambda message: message.text == txt.OPTION_4)
def infrastructure_handler(message):
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    back_button = telebot.types.KeyboardButton(txt.BACK_BUTTON)
    markup.add(back_button)
    bot.send_message(message.chat.id, txt.INFRASTRUCTURE_ANSWER, reply_markup=markup, parse_mode='HTML')


@bot.message_handler(func=lambda message: message.text == txt.BACK_BUTTON)
def back_handler(message):
    main_menu(message)


bot.polling(none_stop=True, interval=0)
